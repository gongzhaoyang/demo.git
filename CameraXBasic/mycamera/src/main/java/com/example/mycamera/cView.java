package com.example.mycamera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

public class cView extends View {
//    private RectF rectF;
//    private Paint mPaint;
    private Paint mPointPaint;
    private static final String TAG = "cView";
    int padding = 50;

//    private int endAngle = 0;
//    private int AngleStep = 1;

    private RectF mScanRectF;
    private int step = 4;
    private int offset = 0;
    private int scannerHigh = 20;
    private RadialGradient radialGradient;

    public cView(Context context) {
        super(context);
        Log.i(TAG, "cView: 1");
//        init();
    }

    public cView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Log.i(TAG, "cView: 2");
//        init();
    }

    public cView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.i(TAG, "cView: 3");
//        init();
    }

    void init(){
        if (mScanRectF != null){
            return;
        }
//        float x = getX();
//        float y = getY();

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();


//        float start = x+padding;
//        float end = width - padding;
//        float top = y + padding;
//        float bottom = height -padding;

//        rectF = new RectF(padding,padding,end,bottom);//左上右下

        mScanRectF = new RectF(padding,padding,width-padding,padding + scannerHigh);


//        mPaint = new Paint();
//        mPaint.setColor(Color.RED);
        mPointPaint = new Paint();
        mPointPaint.setColor(Color.BLUE);
        mPointPaint.setAntiAlias(true);


    }

    @Override
    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
        init();
//        canvas.drawCircle(0,0,5,mPointPaint);
//        canvas.drawCircle(getWidth(),getHeight(),5,mPointPaint);
//        canvas.drawCircle(getX(),getY() + getMeasuredHeight(),5,mPointPaint);
//        canvas.drawCircle(getX()+ getMeasuredWidth(),getY() + getMeasuredHeight(),5,mPointPaint);
//        canvas.drawOval(rectF,mPaint);
//        canvas.drawArc(rectF,90,endAngle,true,mPointPaint);
        canvas.drawOval(mScanRectF,mPointPaint);
        drawScanner(canvas);
        invalidate();


//        endAngle = endAngle + AngleStep;
//        if (endAngle == 360){
//            endAngle = 0;
//        }
    }

    void drawScanner(Canvas canvas){
        radialGradient =new RadialGradient((float) getWidth()/2,padding + offset + (float)scannerHigh/2,360f,
                new int[]{Color.GREEN, Color.TRANSPARENT, Color.TRANSPARENT },null, Shader.TileMode.MIRROR);

        ComposeShader composeShader = new ComposeShader(radialGradient, radialGradient, PorterDuff.Mode.ADD);

        mPointPaint.setShader(composeShader);
        mScanRectF.set(padding,padding + offset,getWidth() - 10,padding + offset + scannerHigh );//左上右下
        offset = offset + step;
        if (offset >= getHeight() - padding*2){
            offset =0;
        }

    }

}
